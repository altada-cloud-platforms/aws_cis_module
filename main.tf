data "aws_caller_identity" "current" {}
data "aws_region" "current" {}

locals {
  all_controls = {
    UnauthorizedAPICalls = {
      pattern     = "{($.errorCode=\"*UnauthorizedOperation\") || ($.errorCode=\"AccessDenied*\")}"
      description = "Monitoring unauthorized API calls will help reveal application errors and may reduce time to detect malicious activity."
    }

    NoMFAConsoleSignin = {
      pattern     = "{ ($.eventName = \"ConsoleLogin\") && ($.additionalEventData.MFAUsed != \"Yes\") && ($.userIdentity.type = \"IAMUser\") && ($.responseElements.ConsoleLogin = \"Success\") }"
      description = "Monitoring for single-factor console logins will increase visibility into accounts that are not protected by MFA."
    }

    RootUsage = {
      pattern     = "{$.userIdentity.type=\"Root\" && $.userIdentity.invokedBy NOT EXISTS && $.eventType !=\"AwsServiceEvent\"}"
      description = "Monitoring for root account logins will provide visibility into the use of a fully privileged account and an opportunity to reduce the use of it."
    }

    IAMChanges = {
      pattern     = "{($.eventName=DeleteGroupPolicy) || ($.eventName=DeleteRolePolicy) || ($.eventName=DeleteUserPolicy) || ($.eventName=PutGroupPolicy) || ($.eventName=PutRolePolicy) || ($.eventName=PutUserPolicy) || ($.eventName=CreatePolicy) || ($.eventName=DeletePolicy) || ($.eventName=CreatePolicyVersion) || ($.eventName=DeletePolicyVersion) || ($.eventName=AttachRolePolicy) || ($.eventName=DetachRolePolicy) || ($.eventName=AttachUserPolicy) || ($.eventName=DetachUserPolicy) || ($.eventName=AttachGroupPolicy) || ($.eventName=DetachGroupPolicy)}"
      description = "Monitoring changes to IAM policies will help ensure authentication and authorization controls remain intact."
    }

    CloudTrailCfgChanges = {
      pattern     = "{($.eventName=CreateTrail) || ($.eventName=UpdateTrail) || ($.eventName=DeleteTrail) || ($.eventName=StartLogging) || ($.eventName=StopLogging)}"
      description = "Monitoring changes to CloudTrail's configuration will help ensure sustained visibility to activities performed in the AWS account."
    }

    ConsoleSigninFailures = {
      pattern     = "{($.eventName=ConsoleLogin) && ($.errorMessage=\"Failed authentication\")}"
      description = "Monitoring failed console logins may decrease lead time to detect an attempt to brute force a credential, which may provide an indicator, such as source IP, that can be used in other event correlation."
    }

    DisableOrDeleteCMK = {
      pattern     = "{($.eventSource=kms.amazonaws.com) && (($.eventName=DisableKey) || ($.eventName=ScheduleKeyDeletion))}"
      description = "Data encrypted with disabled or deleted keys will no longer be accessible."
    }

    S3BucketPolicyChanges = {
      pattern     = "{($.eventSource=s3.amazonaws.com) && (($.eventName=PutBucketAcl) || ($.eventName=PutBucketPolicy) || ($.eventName=PutBucketCors) || ($.eventName=PutBucketLifecycle) || ($.eventName=PutBucketReplication) || ($.eventName=DeleteBucketPolicy) || ($.eventName=DeleteBucketCors) || ($.eventName=DeleteBucketLifecycle) || ($.eventName=DeleteBucketReplication))}"
      description = "Monitoring changes to S3 bucket policies may reduce time to detect and correct permissive policies on sensitive S3 buckets."
    }

    AWSConfigChanges = {
      pattern     = "{($.eventSource=config.amazonaws.com) && (($.eventName=StopConfigurationRecorder) || ($.eventName=DeleteDeliveryChannel) || ($.eventName=PutDeliveryChannel) || ($.eventName=PutConfigurationRecorder))}"
      description = "Monitoring changes to AWS Config configuration will help ensure sustained visibility of configuration items within the AWS account."
    }

    SecurityGroupChanges = {
      pattern     = "{($.eventName=AuthorizeSecurityGroupIngress) || ($.eventName=AuthorizeSecurityGroupEgress) || ($.eventName=RevokeSecurityGroupIngress) || ($.eventName=RevokeSecurityGroupEgress) || ($.eventName=CreateSecurityGroup) || ($.eventName=DeleteSecurityGroup)}"
      description = "Monitoring changes to security group will help ensure that resources and services are not unintentionally exposed."
    }

    NACLChanges = {
      pattern     = "{($.eventName=CreateNetworkAcl) || ($.eventName=CreateNetworkAclEntry) || ($.eventName=DeleteNetworkAcl) || ($.eventName=DeleteNetworkAclEntry) || ($.eventName=ReplaceNetworkAclEntry) || ($.eventName=ReplaceNetworkAclAssociation)}"
      description = "Monitoring changes to NACLs will help ensure that AWS resources and services are not unintentionally exposed."
    }

    NetworkGWChanges = {
      pattern     = "{($.eventName=CreateCustomerGateway) || ($.eventName=DeleteCustomerGateway) || ($.eventName=AttachInternetGateway) || ($.eventName=CreateInternetGateway) || ($.eventName=DeleteInternetGateway) || ($.eventName=DetachInternetGateway)}"
      description = "Monitoring changes to network gateways will help ensure that all ingress/egress traffic traverses the VPC border via a controlled path."
    }

    RouteTableChanges = {
      pattern     = "{($.eventName=CreateRoute) || ($.eventName=CreateRouteTable) || ($.eventName=ReplaceRoute) || ($.eventName=ReplaceRouteTableAssociation) || ($.eventName=DeleteRouteTable) || ($.eventName=DeleteRoute) || ($.eventName=DisassociateRouteTable)}"
      description = "Monitoring changes to route tables will help ensure that all VPC traffic flows through an expected path."
    }

    VPCChanges = {
      pattern     = "{($.eventName=CreateVpc) || ($.eventName=DeleteVpc) || ($.eventName=ModifyVpcAttribute) || ($.eventName=AcceptVpcPeeringConnection) || ($.eventName=CreateVpcPeeringConnection) || ($.eventName=DeleteVpcPeeringConnection) || ($.eventName=RejectVpcPeeringConnection) || ($.eventName=AttachClassicLinkVpc) || ($.eventName=DetachClassicLinkVpc) || ($.eventName=DisableVpcClassicLink) || ($.eventName=EnableVpcClassicLink)}"
      description = "Monitoring changes to VPC will help ensure VPC traffic flow is not getting impacted."
    }

    AWSOrganizationsChanges = {
      pattern     = "{ ($.eventSource = organizations.amazonaws.com) && (($.eventName = \"AcceptHandshake\") || ($.eventName = \"AttachPolicy\") || ($.eventName = \"CreateAccount\") || ($.eventName = \"CreateOrganizationalUnit\") || ($.eventName = \"CreatePolicy\") || ($.eventName = \"DeclineHandshake\") || ($.eventName = \"DeleteOrganization\") || ($.eventName = \"DeleteOrganizationalUnit\") || ($.eventName = \"DeletePolicy\") || ($.eventName = \"DetachPolicy\") || ($.eventName = \"DisablePolicyType\") || ($.eventName = \"EnablePolicyType\") || ($.eventName = \"InviteAccountToOrganization\") || ($.eventName = \"LeaveOrganization\") || ($.eventName = \"MoveAccount\") || ($.eventName = \"RemoveAccountFromOrganization\") || ($.eventName = \"UpdatePolicy\") || ($.eventName = \"UpdateOrganizationalUnit\")) }"
      description = "Monitoring AWS Organizations changes can help you prevent any unwanted, accidental or intentional modifications that may lead to unauthorized access or other security breaches. This monitoring technique helps you to ensure that any unexpected changes performed within your AWS Organizations can be investigated and any unwanted changes can be rolled back."
    }
  }

  

  prefix   = var.use_random_name_prefix ? "${random_pet.this[0].id}-" : var.name_prefix
  controls = { for k, v in local.all_controls : k => v if !contains(var.disabled_controls, k) }
}

resource "random_pet" "this" {
  count = var.use_random_name_prefix ? 1 : 0

  length = 2
}


resource "aws_sns_topic" "user_updates" {
  name            = var.sns_topic_name
  delivery_policy = <<EOF
{
  "http": {
    "defaultHealthyRetryPolicy": {
      "minDelayTarget": 20,
      "maxDelayTarget": 20,
      "numRetries": 3,
      "numMaxDelayRetries": 0,
      "numNoDelayRetries": 0,
      "numMinDelayRetries": 0,
      "backoffFunction": "linear"
    },
    "disableSubscriptionOverrides": false,
    "defaultThrottlePolicy": {
      "maxReceivesPerSecond": 1
    }
  }
}
EOF
}

resource "aws_sns_topic_subscription" "user_updates_sqs_target" {
  topic_arn = aws_sns_topic.user_updates.arn
  protocol  = "email"
  endpoint  = var.endpoint
}

resource "aws_s3_bucket" "s3" {
  bucket        = var.s3_bucket_name
  force_destroy = true
}

resource "aws_s3_bucket_policy" "s3" {
  bucket = aws_s3_bucket.s3.id
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "${aws_s3_bucket.s3.arn}"
        },
        {
            "Sid": "AWSCloudTrailWrite",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "${aws_s3_bucket.s3.arn}/prefix/AWSLogs/${data.aws_caller_identity.current.account_id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
POLICY
}

resource "aws_s3_bucket_public_access_block" "s3" {
  bucket = aws_s3_bucket.s3.id

  block_public_acls   = true
  block_public_policy = true
  restrict_public_buckets = true
  ignore_public_acls = true

}

resource "aws_iam_role" "cloudtrail_to_cloudwatch" {
  name = var.cloudtrail_role_name

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "cloudtrail_to_cloudwatch" {
  name        = var.cloudtrail_policy_name

  policy = jsonencode(
    {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailCreateLogStream2014110",
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream"
            ],
            "Resource": [
                "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:${aws_cloudwatch_log_group.cloudwatch.name}:log-stream:${data.aws_caller_identity.current.account_id}_CloudTrail_${data.aws_region.current.name}*"
            ]
        },
        {
            "Sid": "AWSCloudTrailPutLogEvents20141101",
            "Effect": "Allow",
            "Action": [
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:${aws_cloudwatch_log_group.cloudwatch.name}:log-stream:${data.aws_caller_identity.current.account_id}_CloudTrail_${data.aws_region.current.name}*"
            ]
        }
    ]
}
  )
}

resource "aws_iam_role_policy_attachment" "cloudtrail_to_cloudwatch" {
  role       = aws_iam_role.cloudtrail_to_cloudwatch.name
  policy_arn = aws_iam_policy.cloudtrail_to_cloudwatch.arn

}

resource "aws_kms_key" "key" {
  description = "Key for cloudtrail"
  policy = data.aws_iam_policy_document.default.json
}

data "aws_iam_policy_document" "default" {
    version = "2012-10-17"
    statement {
      sid    = "Enable IAM User Permissions"
      effect = "Allow"
      principals {
        type        = "AWS"
        identifiers = ["*"]
      }
      actions   = ["kms:*"]
      resources = ["*"]
    }
    statement {
      sid    = "Allow CloudTrail to encrypt logs"
      effect = "Allow"
      principals {
        type        = "Service"
        identifiers = ["cloudtrail.amazonaws.com"]
      }
      actions   = ["kms:GenerateDataKey*"]
      resources = ["*"]
      condition {
        test     = "StringLike"
        variable = "kms:EncryptionContext:aws:cloudtrail:arn"
        values   = ["arn:aws:cloudtrail:*:XXXXXXXXXXXX:trail/*"]
      }
    }

    statement {
      sid    = "Allow CloudTrail to describe key"
      effect = "Allow"
      principals {
        type        = "Service"
        identifiers = ["cloudtrail.amazonaws.com"]
      }
      actions   = ["kms:DescribeKey"]
      resources = ["*"]
    }

    statement {
      sid    = "Allow principals in the account to decrypt log files"
      effect = "Allow"
      principals {
        type        = "AWS"
        identifiers = ["*"]
      }
      actions = [
        "kms:Decrypt",
        "kms:ReEncryptFrom"
      ]
      resources = ["*"]
      condition {
        test     = "StringEquals"
        variable = "kms:CallerAccount"
        values = [
        "XXXXXXXXXXXX"]
      }
      condition {
        test     = "StringLike"
        variable = "kms:EncryptionContext:aws:cloudtrail:arn"
        values   = ["arn:aws:cloudtrail:*:XXXXXXXXXXXX:trail/*"]
      }
    }

    statement {
      sid    = "Allow alias creation during setup"
      effect = "Allow"
      principals {
        type        = "AWS"
        identifiers = ["*"]
      }
      actions   = ["kms:CreateAlias"]
      resources = ["*"]
    }
  }


resource "aws_cloudtrail" "cloudtrail" {
  name                          = var.cloudtrail_name
  s3_bucket_name                = aws_s3_bucket.s3.id
  s3_key_prefix                 = "prefix"
  is_multi_region_trail = true
  enable_log_file_validation = true
  cloud_watch_logs_group_arn = "${aws_cloudwatch_log_group.cloudwatch.arn}:*"
  cloud_watch_logs_role_arn = aws_iam_role.cloudtrail_to_cloudwatch.arn
  kms_key_id = aws_kms_key.key.arn
}

resource "aws_cloudwatch_log_group" "cloudwatch" {
  name = var.cloudwatch_log_group_name

}

resource "aws_cloudwatch_log_metric_filter" "this" {
  for_each = var.create ? local.controls : {}

  name           = "${local.prefix}${each.key}"
  pattern        = each.value["pattern"]
  log_group_name = aws_cloudwatch_log_group.cloudwatch.name

  metric_transformation {
    name          = "${local.prefix}${each.key}"
    namespace     = lookup(each.value, "namespace", var.namespace)
    value         = 1
    default_value = 0
  }
}

resource "aws_cloudwatch_metric_alarm" "this" {
  for_each = var.create ? local.controls : {}

  metric_name       = aws_cloudwatch_log_metric_filter.this[each.key].id
  namespace         = lookup(each.value, "namespace", var.namespace)
  alarm_name        = "${local.prefix}${each.key}"
  alarm_description = aws_sns_topic.user_updates.arn

  actions_enabled           = lookup(each.value, "actions_enabled", var.actions_enabled)
  alarm_actions             = [aws_sns_topic.user_updates.arn]
  ok_actions                = lookup(each.value, "ok_actions", null)
  insufficient_data_actions = lookup(each.value, "insufficient_data_actions", null)

  comparison_operator                   = lookup(each.value, "comparison_operator", "GreaterThanOrEqualToThreshold")
  evaluation_periods                    = lookup(each.value, "evaluation_periods", 1)
  threshold                             = lookup(each.value, "threshold", 1)
  unit                                  = lookup(each.value, "unit", null)
  datapoints_to_alarm                   = lookup(each.value, "datapoints_to_alarm", null)
  treat_missing_data                    = lookup(each.value, "treat_missing_data", "notBreaching")
  evaluate_low_sample_count_percentiles = lookup(each.value, "evaluate_low_sample_count_percentiles", null)

  period     = lookup(each.value, "period", 300)
  statistic  = lookup(each.value, "statistic", "Sum")
  dimensions = lookup(each.value, "dimensions", null)

  tags = var.tags
}


variable "create" {
  description = "Whether to create the Cloudwatch log metric filter and metric alarms"
  type        = bool
  default     = true
}

variable "use_random_name_prefix" {
  description = "Whether to prefix resource names with random prefix"
  type        = bool
  default     = false
}

variable "name_prefix" {
  description = "A name prefix for the cloudwatch alarm (if use_random_name_prefix is true, this will be ignored)"
  type        = string
  default     = ""
}

variable "disabled_controls" {
  description = "List of IDs of disabled CIS controls"
  type        = list(string)
  default     = []
}

variable "namespace" {
  description = "The namespace where metric filter and metric alarm should be cleated"
  type        = string
  default     = "CISBenchmark"
}

variable "cloudwatch_log_group_name" {
  description = "The name of the log group to associate the metric filter with"
  type        = string
  default     = ""
}

variable "actions_enabled" {
  description = "Indicates whether or not actions should be executed during any changes to the alarm's state."
  type        = bool
  default     = true
}

variable "tags" {
  description = "A mapping of tags to assign to all resources"
  type        = map(string)
  default     = {}
}

variable "endpoint" {
  description = "endpoint for sns topic"
  type = string
  default = ""
}

variable "s3_bucket_name" {
  description = "Name of the Clouftrail bucket"
  type = string
  default = ""
}

variable "cloudtrail_name" {
  description = "Name of the Cloudtrail"
  type = string
  default = ""
}

variable "sns_topic_name" {
  description = "Name of the Sns Topic"
  type = string
  default = ""
}

variable "cloudtrail_role_name" {
  description = "Name of the Cloudtrail Role"
  type = string
  default = ""
}

variable "cloudtrail_policy_name" {
  description = "Name of the Policy"
  type = string
  default = ""
}



